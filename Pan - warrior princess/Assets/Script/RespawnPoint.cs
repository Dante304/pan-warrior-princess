﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RespawnPoint : MonoBehaviour {

	public bool respawnActive;

	void OnTriggerEnter2D(Collider2D collider)
	{
		if (collider.gameObject.tag == "Player" && !respawnActive)
		{
			this.GetComponentInChildren<SpriteRenderer>().flipY = true;
			this.gameObject.GetComponent<AudioSource> ().Play ();
			this.tag = "Respawn";
			respawnActive = true;
		}

	}
}
