﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class HeroMoveLeft : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler {

	public GameObject hero;
	public BoxCollider2D heroHitCollider;
	public float heroSpeed;
	public static bool move;
	private Rigidbody2D heroRigidbody;
	private Animator anim;
	private SpriteRenderer heroSpriteRenderer;

	void Start()
	{
		heroRigidbody = hero.GetComponent<Rigidbody2D> ();
		anim = hero.gameObject.GetComponentInChildren<Animator> ();
		heroSpriteRenderer = hero.gameObject.GetComponentInChildren<SpriteRenderer> ();
	}



	void Update () 
	{
		if (move) 
		{
			heroRigidbody.velocity = new Vector2 (-heroSpeed, heroRigidbody.velocity.y);
		}
		//TODO Należy zoptymalizować chodzenie ponieważ wykonywanie się co klatkę powoduję zjadanie cennych zasobów a zarazem jest nie poprawne dla różnych urządzeń.
	}

	#region IPointerEnterHandler implementation

	public void OnPointerEnter (PointerEventData eventData)
	{
		move = true;
		heroSpriteRenderer.flipX = true;
		if (heroHitCollider.offset.x > 0) 
		{
			heroHitCollider.offset = new Vector2 (heroHitCollider.offset.x * -1, heroHitCollider.offset.y);
		}

		anim.SetBool ("Walk", move);
	}

	#endregion

	#region IPointerExitHandler implementation

	public void OnPointerExit (PointerEventData eventData)
	{
		move = false;
		heroRigidbody.velocity = new Vector2 (0, heroRigidbody.velocity.y);
		anim.SetBool ("Walk", move);
	}

	#endregion
}
