﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ConversationClass : MonoBehaviour 
{
	public List<Sprite> imageOfPersonWhoTalk;
	public List<string> linesOfText;
	public List<string> linesOfWhoTalk;
}
