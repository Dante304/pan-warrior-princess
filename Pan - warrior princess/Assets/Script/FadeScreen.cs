﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FadeScreen : MonoBehaviour {

	public float splashScreenSpeed;
	public CanvasGroup canvasGroup;
	
	// Update is called once per frame
	void Update () 
	{
		//TODO poprawić FadeScreen aby nie robił się co klatkę.
		if (canvasGroup.alpha < 1) 
		{
			canvasGroup.alpha = canvasGroup.alpha + splashScreenSpeed * Time.deltaTime;
		} 
	}
}
