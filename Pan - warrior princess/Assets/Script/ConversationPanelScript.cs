﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Text.RegularExpressions;
using System;

public class ConversationPanelScript : MonoBehaviour 
{
	public bool isActive; 
	public ConversationClass conversationClass;
	public Text conversationPanelText;
	public Text personOfWhoTalkSignature;
	public Image firstPersonImage;
	public Image secondPersonImage;
	public Int16 conversationLine;
	public Int16 nameLine;

	void Start () 
	{
		TextToPanel(conversationLine,nameLine );
	}

	void Update()
	{
		//TODO do refaktoryzacji by nie było sprawdzane co klatkę.
		/*
		if (Input.GetMouseButtonDown(0)) 
		{
			ChangeText ();
		}
		*/
	}

	public void ChangeText()
	{
		conversationLine++;
		nameLine++;
		TextToPanel (conversationLine, nameLine);
			
	}

	void TextToPanel(Int16 _conversationLine , Int16 _nameLine )
	{
		if (_conversationLine >= conversationClass.linesOfText.Count) 
		{
			isActive = false;
			this.gameObject.GetComponent<ConversationTrigger> ().ConverstionPanel.SetActive (false);
			UiButtons.TurnOnButtons ();
			Time.timeScale = 1;
			return;
		}

		conversationPanelText.GetComponent<Text>().text = conversationClass.linesOfText [_conversationLine];
		personOfWhoTalkSignature.GetComponent<Text>().text  = conversationClass.linesOfWhoTalk[_nameLine];
		var reg = Regex.Match (conversationPanelText.GetComponent<Text> ().text,"Image1:([0-9]{1})");
		var reg2 = Regex.Match (conversationPanelText.GetComponent<Text> ().text,"Image2:([0-9]{1})");

		if (reg != null && reg.Length > 0 && reg.Value != "") 
		{
			var whatFirstImage = reg.Groups[1].Value;
			firstPersonImage.sprite = conversationClass.imageOfPersonWhoTalk [Convert.ToInt32 (whatFirstImage)];
		}
		if (reg2 != null && reg2.Length > 0 && reg2.Value != "") 
		{
			var whatSecondImage = reg2.Groups[1].Value;
			secondPersonImage.sprite = conversationClass.imageOfPersonWhoTalk [Convert.ToInt32 (whatSecondImage)];
		}

		conversationPanelText.GetComponent<Text>().text = Regex.Replace(conversationPanelText.GetComponent<Text> ().text, "Image1:[0-9]{1}" , "");
		conversationPanelText.GetComponent<Text>().text = Regex.Replace(conversationPanelText.GetComponent<Text> ().text, "Image2:[0-9]{1}" , "");

	}

}
