﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SplashScreenScript : MonoBehaviour {

	public float splashScreenSpeed;
	public float splashScreenTime;
	public string scenToLoad;
	public CanvasGroup canvasGroup;
	private bool darker;
	AsyncOperation async;

	void Start () 
	{
		Invoke ("EndSplashtScreen", splashScreenTime);
	}

	void Update()
	{
		if (canvasGroup.alpha >= 1) 
		{
			darker = true;
		}
		if (canvasGroup.alpha < 1 && darker == false) 
		{
			canvasGroup.alpha = canvasGroup.alpha + splashScreenSpeed * Time.deltaTime;
		} 
		else 
		{
			canvasGroup.alpha = canvasGroup.alpha - splashScreenSpeed * Time.deltaTime;
		}
	}

	void EndSplashtScreen()
	{
		StartCoroutine(LoadingScreen(scenToLoad));
	}

	IEnumerator LoadingScreen(string scenName)
	{
		async = SceneManager.LoadSceneAsync (scenName);
		async.allowSceneActivation = false;

		while (async.isDone == false) 
		{
			if (async.progress == 0.9f) 
			{
				async.allowSceneActivation = true;
			}
			yield return null;
		}
	}
}
