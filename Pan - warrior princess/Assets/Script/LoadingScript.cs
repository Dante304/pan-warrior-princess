﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LoadingScript : MonoBehaviour {

	public GameObject loadingScreenObj;
	public Slider slider;
	AsyncOperation async;

	void Start()
	{
		var scenToLoad = PlayerPrefs.GetString ("ScenToLoad");
		Loading(scenToLoad);
	}

	public void Loading(string scenName)
	{
		StartCoroutine(LoadingScreen(scenName));
	}

	IEnumerator LoadingScreen(string scenName)
	{
		async = SceneManager.LoadSceneAsync (scenName);
		async.allowSceneActivation = false;

		while (async.isDone == false) 
		{
			slider.value = async.progress;
			if (async.progress == 0.9f) 
			{
				slider.value = 1f;
				async.allowSceneActivation = true;
			}
			yield return null;
		}
	}


}
