﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicChanger : MonoBehaviour {

	public AudioClip musicToPlay;
	public float volume;


	// Use this for initialization
	void Start () 
	{
		ChangeMusic ();
	}

	public void ChangeMusic()
	{
		var audioSource = GameObject.Find ("Audio").GetComponent<AudioSource> ();
		audioSource.clip = musicToPlay;
		audioSource.volume = volume;


		audioSource.Play ();
	}

}
