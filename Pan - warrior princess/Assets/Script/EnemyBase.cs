﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBase : MonoBehaviour {

	public float health;
	public float attackSpeed;
	public float getDamageTime;
	public bool getingDamage;
	public bool inRange;
	public bool closeUpAttack;
	public bool rangeAttack;
	public AudioClip damageSound;
	public GameObject enemyHealth;
	public GameObject bloodEffect;
	public GameObject heart;
	/// <summary>
	/// Jeżeli przeciwnik strzela pociskami należy w to miejsce umieścić pocisk.
	/// </summary>
	public GameObject bullet;

	private AudioSource audioSource;
	private SpriteRenderer enemySpriteRenderer;
	private Rigidbody2D enemyRigidBody;
	private Animator animator;
	private GameObject hero;

	void Start()
	{
		enemyRigidBody = this.gameObject.GetComponent<Rigidbody2D> ();
		hero = HeroStats.aboutHero;
		audioSource = this.GetComponent<AudioSource> ();
		animator = this.gameObject.GetComponentInChildren<Animator> ();
	}
		
	void OnTriggerEnter2D(Collider2D collider)
	{
		if (collider.gameObject.tag == "AttackDistance") 
		{
			if (!getingDamage)
			{
				getingDamage = true;
				GetDamage ();
			}
		}

		#region Atak
		if (closeUpAttack) 
		{
			if (collider.gameObject.tag == "Player")
			{
				inRange = true;
				this.GetComponent<EnemyMove>().move  = false;
				animator.SetBool ("Attack", true);
			} 
		}
		if (rangeAttack) 
		{
			if (collider.gameObject.tag == "Player")
			{
				Instantiate(bullet);
			} 
		}
		#endregion
	}


	#region Przyjmowanie obrażeń


	void GetDamage()
	{
		Debug.Log ("Przeciwnik otrzymał obrażenia");

		//audioSource.clip = damageSound;
		//audioSource.Play ();

		health--;
		this.GetComponent<EnemyMove>().move  = false;

		ShowHealth (health);
		Instantiate (bloodEffect, transform.position, Quaternion.identity);
		enemyRigidBody.velocity = new Vector2 (0, enemyRigidBody.velocity.y);

		if (health <= 0) 	
		{
			Death ();
		} 
		else 
		{
			animator.SetBool ("GetDamage", true);
			Invoke ("EndGetDamage", getDamageTime);
		}
	}

	void EndGetDamage()
	{
		getingDamage = false;
		animator.SetBool ("GetDamage", false);
		this.GetComponentInChildren<Animator> ().SetBool ("GetDamage", false);
		this.GetComponent<EnemyMove>().move = true;
	}

	void ShowHealth(float health)
	{
		enemyHealth.SetActive (true);
		var childrenOfEnemyHealth = enemyHealth.GetComponentsInChildren<Transform> ();


		for (int i = 0; i < childrenOfEnemyHealth.Length; i++)
		{

			if (childrenOfEnemyHealth[i].gameObject.tag != "EnemyHealth")
			{
				Destroy (childrenOfEnemyHealth [i].gameObject);
			}
		}

		for (int i = 0; i < health; i++) 
		{
			GameObject placedObject = Instantiate (heart);
			var newPosition = enemyHealth.transform.position;
			newPosition.x = newPosition.x + i;
			placedObject.transform.position = newPosition;
			placedObject.transform.SetParent (enemyHealth.transform);
			Invoke ("HideHealth", 1);
		}
	}

	void HideHealth()
	{
		enemyHealth.SetActive (false);
	}

	void Death()
	{
		this.GetComponent<EnemyMove>().move  = false;
		this.gameObject.tag = "DeadEnemy";
		var tableOfCollidors = this.gameObject.GetComponents<BoxCollider2D> ();
		for (int i = 0; i < tableOfCollidors.Length; i++) 
		{
			if (tableOfCollidors [i].isTrigger == true) 
			{
				Destroy (tableOfCollidors [i]);
			}
		}

		animator.SetTrigger ("Dead");
	}

	#endregion

	void OnTriggerExit2D(Collider2D collider)
	{
		if (collider.gameObject.tag == "Player") 
		{
			inRange = false;
			animator.SetBool ("Attack", false);
			this.GetComponent<EnemyMove>().move  = true;
		}
	}



}
