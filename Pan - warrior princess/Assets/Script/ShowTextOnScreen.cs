﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShowTextOnScreen : MonoBehaviour {

	public GameObject panelWithText;
	public string textToShow;
	public string nameOfTheSpeaker;

	void OnTriggerEnter2D(Collider2D collider)
	{
		if (collider.gameObject.tag == "Player") 
		{
			panelWithText.SetActive (true);
			panelWithText.gameObject.GetComponentInChildren<Text> ().text = textToShow;
			for (int i = 0; i < panelWithText.transform.childCount; i++) 
			{
				if (panelWithText.transform.GetChild(i).CompareTag("SpeakerName"))
				{
					panelWithText.transform.GetChild (i).GetComponentInChildren<Text> ().text = nameOfTheSpeaker;
				}

			}

		} 
	}

	void OnTriggerExit2D(Collider2D collider)
	{
		if (collider.gameObject.tag == "Player") 
		{
			panelWithText.SetActive (false);
			panelWithText.gameObject.GetComponentInChildren<Text> ().text = "";
		}
	}
}
