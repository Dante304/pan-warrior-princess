﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class LoadCoins : MonoBehaviour 
{
	private GameObject coinValueText;

	void Start () 
	{
		coinValueText = GameObject.FindGameObjectWithTag ("Money");
		coinValueText.GetComponent<Text> ().text = Convert.ToString (PlayerPrefs.GetFloat ("CoinAll"));
	}
	
}
