﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageHero : MonoBehaviour 
{
	public GameObject hero;
	public GameObject enemy;
	public bool turnOffTrigger;

	void OnTriggerEnter2D(Collider2D collider)
	{
		if (collider.gameObject.tag == "Player" && !turnOffTrigger) 
		{
			HitHero (hero);
		}
	}

	public void HitHero(GameObject hero)
	{
		float numberOfHearts = hero.GetComponent<HeroStats> ().GetSetNumberOfHearts;
		Debug.Log ("Liczba serc ktore posiada to " + numberOfHearts );
		numberOfHearts--;
		hero.GetComponent<HeroStats> ().GetSetNumberOfHearts = numberOfHearts;
	}




}
