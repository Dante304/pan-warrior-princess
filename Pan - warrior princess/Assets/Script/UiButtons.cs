﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UiButtons : MonoBehaviour {

	static public List<GameObject> listOfButtons;
	public List<GameObject> listOfButtonPublic;

	void Start()
	{
		listOfButtons = listOfButtonPublic;
	}

	#region  TurnOnButtons methods

	static public void TurnOnButtons()
	{
		for (int button = 0; button < listOfButtons.Count; button++) 
		{
			listOfButtons [button].SetActive (true);
			if (listOfButtons [button].CompareTag("LeftMove")) 
			{
				listOfButtons [button].GetComponent<HeroMoveLeft> ().OnPointerExit (null);

			}
			else if (listOfButtons [button].CompareTag("RightMove")) 
			{
				listOfButtons [button].GetComponent<HeroMoveRight> ().OnPointerExit (null);
			}
		}
	}

	/// <summary>
	/// Przeciążona metoda która przyjmuję jakie klawisze mają zostać włączone.
	/// </summary>
	/// <param name="listOfButtons">List of buttons.</param>
	static public void TurnOnButtons(List<GameObject> listOfButtons)
	{
		for (int button = 0; button < listOfButtons.Count; button++) 
		{
			listOfButtons [button].SetActive (true);
			if (listOfButtons [button].CompareTag("LeftMove")) 
			{
				listOfButtons [button].GetComponent<HeroMoveLeft> ().OnPointerExit (null);

			}
			else if (listOfButtons [button].CompareTag("RightMove")) 
			{
				listOfButtons [button].GetComponent<HeroMoveRight> ().OnPointerExit (null);
			}
		}
	}

	#endregion

	#region TurnOffButtons methods

	static public void TurnOffButtons()
	{
		for (int button = 0; button < listOfButtons.Count; button++) 
		{
			listOfButtons [button].SetActive (false);
			if (listOfButtons [button].CompareTag("LeftMove")) 
			{
				listOfButtons [button].GetComponent<HeroMoveLeft> ().OnPointerExit (null);

			}
			else if (listOfButtons [button].CompareTag("RightMove")) 
			{
				listOfButtons [button].GetComponent<HeroMoveRight> ().OnPointerExit (null);
			}
		}
	}
		
	/// <summary>
	/// Przeciążona metoda która przyjmuję jakie klawisze mają zostać wyłączone.
	/// </summary>
	/// <param name="listOfButtons">List of buttons.</param>
	static public void TurnOffButtons(List<GameObject> listOfButtons)
	{
		for (int button = 0; button < listOfButtons.Count; button++) 
		{
			listOfButtons [button].SetActive (false);
			if (listOfButtons [button].CompareTag("LeftMove")) 
			{
				listOfButtons [button].GetComponent<HeroMoveLeft> ().OnPointerExit (null);

			}
			else if (listOfButtons [button].CompareTag("RightMove")) 
			{
				listOfButtons [button].GetComponent<HeroMoveRight> ().OnPointerExit (null);
			}
		}
	}
		
	#endregion
}
