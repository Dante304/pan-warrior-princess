﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMoveAgain : MonoBehaviour
{
	public void MoveAgain()
	{
		this.GetComponentInParent<EnemyMove> ().move = true;
		this.GetComponent<Animator> ().SetBool ("GetDamage", false);
	}
}