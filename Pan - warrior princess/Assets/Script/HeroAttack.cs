﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class HeroAttack : MonoBehaviour, IPointerEnterHandler 
{
	
	public bool attackTime;
	public AudioClip atackSound;
	private AudioSource audioSourceHero;
	private GameObject hero;

	void Start()
	{
		hero = GameObject.FindGameObjectWithTag ("Player");
		audioSourceHero = this.GetComponent<AudioSource> ();
	}

	public void OnPointerEnter (PointerEventData eventData)
	{
		if (!attackTime) 
		{
			AttackClick ();
		}
	}


	void AttackClick()
	{
		if (!attackTime) 
		{
			audioSourceHero.clip = atackSound;
			hero.GetComponent<DamageToEnemy> ().Attack ();
		}
		//TODO Wysłanie wiadomości do wszystkich obiektów o tagu enemy że któryś znajduje się w zasięgu i jest atakowany następnie kazdy z przeciwnikow sprawdza czy jest w zasiegu i zadaje obrazenia temu ktory jest w zasiegu.


	}
}
