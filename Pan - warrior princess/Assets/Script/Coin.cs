﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class Coin : MonoBehaviour 
{

	public float coinValue;
	private GameObject coinText;


	void Start()
	{
		coinText = GameObject.FindWithTag("Money");
	}


	void OnTriggerEnter2D(Collider2D collider)
	{
		
		if (collider.gameObject.tag == "Player") 
		{
			float heroMoney=GetValueOfTheMoney ();

			this.gameObject.GetComponentInParent<AudioSource> ().Play ();  //Ta linia została dodana przezemnie (Kamil :) ) odpowiada za znalezienie komponentu AudioSource w obiekcie rodzica i odpalenie dzwięku monety

			coinText.GetComponent<Text> ().text = Convert.ToString (heroMoney);
			PlayerPrefs.SetFloat ("CoinAll", heroMoney);
			Destroy (this.gameObject);
		}

	
	}

	float GetValueOfTheMoney()
	{
		float heroMoney =Convert.ToSingle(coinText.GetComponent<Text> ().text);
		return heroMoney = heroMoney + coinValue;
	}
}
