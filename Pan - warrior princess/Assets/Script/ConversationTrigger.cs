﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ConversationTrigger : MonoBehaviour 
{
	public GameObject ConverstionPanel;
	public ConversationPanelScript conversationPanelScirpt;

	void OnTriggerEnter2D(Collider2D collider)
	{
		if (collider.gameObject.tag == "Player" && conversationPanelScirpt.isActive) 
		{
			Time.timeScale = 0;
			UiButtons.TurnOffButtons ();
			ConverstionPanel.SetActive (true);
			Button tapToContinue = ConverstionPanel.GetComponentInChildren<Button> ();
			tapToContinue.onClick.AddListener (conversationPanelScirpt.ChangeText);
		}
	}

	void OnTriggerExit2D(Collider2D collider)
	{
		if (collider.gameObject.tag == "Player"  && conversationPanelScirpt.isActive) 
		{
			Button tapToContinue = ConverstionPanel.GetComponentInChildren<Button> ();
			tapToContinue.onClick.RemoveAllListeners ();
			ConverstionPanel.SetActive (false);
		}
	}
}
