﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ButtonPress : MonoBehaviour
{
	public string scenToLoad;
	public bool turnOnLoadScreen;

	public void LoadScen()
	{
		if (turnOnLoadScreen) 
		{
			PlayerPrefs.SetString ("ScenToLoad", scenToLoad);
			SceneManager.LoadScene ("LoadScreen");
		}
		else if (!turnOnLoadScreen) 
		{
			SceneManager.LoadScene (scenToLoad);
		}

	
	}

	public void ExitGame()
	{
		Application.Quit();
	}

}
