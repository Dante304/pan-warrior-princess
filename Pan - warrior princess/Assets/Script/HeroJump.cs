﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class HeroJump : MonoBehaviour, IPointerEnterHandler
{
	public GameObject hero;
	public float jumpPower;
	public Transform groundTesting; //zmienna posiadająca kordynaty bohaterki i sprawdzająca czy styka się z podłożem.
	public LayerMask testingLayers;
	public AudioClip jumpSound;
	private AudioSource audioSourceHero;
	private bool onGround;
	private int jumpCount = 0;

	void Start()
	{
		audioSourceHero = this.GetComponent<AudioSource> ();
	}

	void Update()
	{
		onGround = Physics2D.OverlapCircle(groundTesting.position,0.1f, testingLayers); //zwracana jest wartość true/false zależnie czy obiekt w danej warstwie o podanych współżędnych w określonej odległości z czymś koliduje
		if (onGround) 
		{
			jumpCount = 0;
		}	
	}

	#region IPointerEnterHandler implementation

	public void OnPointerEnter (PointerEventData eventData)
	{
		if (onGround) 
		{
			audioSourceHero.clip = jumpSound;
			audioSourceHero.Play ();
			hero.GetComponent<Rigidbody2D> ().AddForce (new Vector2(0, jumpPower));
		}
		if (!onGround && jumpCount < 1) 
		{
			audioSourceHero.clip = jumpSound;
			audioSourceHero.Play ();
			jumpCount++;
			hero.GetComponent<Rigidbody2D> ().velocity = new Vector2 (hero.GetComponent<Rigidbody2D> ().velocity.x, 0); //wyzerowuje siłę działającą na bohaterke aby jej nie potrzebnie zwielokrotnić
			hero.GetComponent<Rigidbody2D> ().AddForce (new Vector2(0, jumpPower)); //nadajemy siłę odziałowującą na bohaterke.
		}

	}

	#endregion




}
