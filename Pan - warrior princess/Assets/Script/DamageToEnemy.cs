﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageToEnemy : MonoBehaviour 
{
	public GameObject AttackBoxCollidor;
	public float attackTime;
	public AudioSource audioSourceHero;

	private GameObject attackButton;
	private GameObject hero;
	private HeroAttack heroAttack;
	private bool canAttack;
	private Animator anim;


	void Start()
	{
		hero = GameObject.FindGameObjectWithTag ("Player");
		anim = hero.gameObject.GetComponentInChildren<Animator> ();	
		attackButton = GameObject.FindGameObjectWithTag ("AttackButton");
		heroAttack = attackButton.GetComponent<HeroAttack> ();
		canAttack = true;
	}


	public void Attack()
	{
		if (canAttack) 
		{
			anim.ResetTrigger ("StopAttack"); // Reset triggera StopAttack ponieważ występował bug jeśli gracz umierał w momencie kiedy ten trigger był aktywny. Powodowało to że ten trigger ciągle był aktywny co podczas animacji ataku wyłączało ją od razu.
			audioSourceHero.Play ();
			anim.SetTrigger ("Attack");
			AttackBoxCollidor.SetActive (true);
			canAttack = false;
			Invoke ("HideAttackBoxCollidor", attackTime);
		}
	}


	public void HideAttackBoxCollidor()
	{
		anim.SetTrigger ("StopAttack");
		AttackBoxCollidor.SetActive (false);
		canAttack = true;

	}

}
