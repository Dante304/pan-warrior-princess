﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class HeroRespawn : MonoBehaviour 
{

	private Animator anim;

	void Start()
	{
		anim = this.gameObject.GetComponentInChildren<Animator> ();
	}

	// Use this for initialization
	public void Respawn () 
	{
		GameObject[] respawnPoints = GameObject.FindGameObjectsWithTag ("Respawn");
		anim.SetTrigger ("Respawn");
		var newPosition = respawnPoints [respawnPoints.Length - 1].transform.position;
		newPosition.z = this.gameObject.transform.position.z;
		this.gameObject.transform.position = newPosition;
		UiButtons.TurnOnButtons ();
		this.GetComponent<HeroStats> ().GetSetNumberOfHearts = 3;
		this.GetComponent<HeroStats> ().GetSetNumberOfEnergyBalls = 3;
		this.GetComponent<HeroStats> ().dead = false;
	}
}
