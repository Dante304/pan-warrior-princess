﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
public class HeroMotionCorrection : MonoBehaviour
{

	public Transform groundTesting; //zmienna posiadająca kordynaty bohaterki i sprawdzająca czy styka się z podłożem.
	public LayerMask testingLayers;
	Rigidbody2D heroRigidbody2D;


	void Start()
	{
		heroRigidbody2D = this.gameObject.GetComponent<Rigidbody2D> ();
	}


	#region IPointerEnterHandler implementation

	void OnTriggerEnter2D(Collider2D collider)
	{
		if (Physics2D.OverlapCircle(groundTesting.position,0.1f, testingLayers) && collider.gameObject.layer == 9) 
		{
			Debug.Log ("Dotknięcie ziemi przez ground");
			heroRigidbody2D.velocity = Vector2.zero;
		}
	}

	#endregion
}
