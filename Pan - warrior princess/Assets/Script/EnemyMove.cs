﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMove : MonoBehaviour 
{
	public bool move;
	public float moveSpeed;
	public BoxCollider2D hitCollider;
	public GameObject pointToMove;
	private float direction;
	private SpriteRenderer enemySpriteRenderer;
	private Vector2 startPosition;
	private Vector2 movePosition;
	private Vector2 enemyPosition;
	private Rigidbody2D enemyRigidBody;
	private bool moveInfo;
	private Animator animator;

	void Start()
	{
		animator = this.gameObject.GetComponentInChildren<Animator> ();
		startPosition = this.gameObject.GetComponent<Transform> ().position;
		movePosition = pointToMove.gameObject.GetComponent<Transform> ().position;
		direction = 1; //TODO Poprawić aby przeciwnik mógł poruszać się nie tylko z prawej na lewo i z powrotem ale też z lewej na prawo.
		enemySpriteRenderer = this.gameObject.GetComponentInChildren<SpriteRenderer> ();
		enemyRigidBody = this.gameObject.GetComponent<Rigidbody2D> ();
		Destroy (pointToMove);
	}


	void Update()
	{
		moveInfo = animator.GetCurrentAnimatorStateInfo (0).IsName ("WalkEnemy");

		if (move) 
		{
			EnemyWalk ();
		}
		else if (!move)
		{
			enemyRigidBody.velocity = new Vector2 (0, enemyRigidBody.velocity.y);
		}
	}

	#region Ruch przeciwnika
	void EnemyWalk()
	{
		if (!moveInfo) 
		{
			animator.SetTrigger ("EnemyWalk");
		}

		enemyRigidBody.velocity = new Vector2 (moveSpeed * direction, enemyRigidBody.velocity.y);
		Vector2 positionNow = this.transform.position;
		if (positionNow.x > movePosition.x) 
		{
			enemySpriteRenderer.flipX = true;
			direction = -1;
			if (hitCollider.offset.x > 0)
			{
				hitCollider.offset = new Vector2 (hitCollider.offset.x * -1, hitCollider.offset.y);
			}
		}
		if(positionNow.x < startPosition.x)
		{
			enemySpriteRenderer.flipX = false;
			direction = 1;
			if (hitCollider.offset.x < 0)
			{
				hitCollider.offset = new Vector2 (hitCollider.offset.x * -1, hitCollider.offset.y);
			}
		}

		//transform.position = Vector2.Lerp (startPosition, movePosition, Mathf.PingPong (Time.time * moveSpeed, 1));
	}
	#endregion
}
