﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Platform : MonoBehaviour {

	// Use this for initialization
	void Start () 
	{
		
	}
	
	void OnTriggerEnter2D(Collider2D other)
	{
		Debug.Log ("Coś wpadło na platformy");
		if (other.tag == "Player") 
		{
			other.transform.parent = transform;
			Debug.Log ("Przyczepiony do platformy");
		}
	}

	void OnTriggerExit2D(Collider2D other)
	{
		if (other.tag == "Player") 
		{
			other.transform.parent = null;
			Debug.Log ("Odczepiony od platformy");
		}
	}
}
