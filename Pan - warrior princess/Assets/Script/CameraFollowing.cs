﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollowing : MonoBehaviour {

	public float speedOfFollowingCamera;
	private GameObject hero;
	private Vector3 positionOfHero;
	private Vector3 currentVelocity;
	// Use this for initialization
	void Start () 
	{
		hero = GameObject.FindGameObjectWithTag ("Player");
	}
	
	// Update is called once per frame
	void Update () 
	{
		positionOfHero = new Vector3 (hero.transform.position.x, hero.transform.position.y + 5, this.gameObject.transform.position.z);
		this.gameObject.transform.position = Vector3.SmoothDamp(this.gameObject.transform.position, positionOfHero, ref currentVelocity, speedOfFollowingCamera );
	}
}
