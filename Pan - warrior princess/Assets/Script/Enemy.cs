﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Enemy :  MonoBehaviour
{

	public float health;
	public float moveSpeed;
	public float attackSpeed;
	public float getDamageTime;
	public bool move;
	public bool getingDamage;
	public GameObject heart;
	public GameObject pointToMove;
	public BoxCollider2D hitCollider;
	public bool attackStart;
	public AudioClip damageSound;
	public GameObject enemyHealth;
	public GameObject bloodEffect;

	private AudioSource audioSource;
	private Vector2 startPosition;
	private Vector2 movePosition;
	private Vector2 enemyPosition;
	private float direction;
	private SpriteRenderer enemySpriteRenderer;
	private Rigidbody2D enemyRigidBody;
	private Animator anim;
	private GameObject hero;
	private DamageHero damageHero;



	void Start()
	{
		hero = GameObject.FindGameObjectWithTag ("Player");
		audioSource = this.GetComponent<AudioSource> ();
		startPosition = this.gameObject.GetComponent<Transform> ().position;
		movePosition = pointToMove.gameObject.GetComponent<Transform> ().position;
		direction = 1; //TODO Poprawić aby przeciwnik mógł poruszać się nie tylko z prawej na lewo i z powrotem ale też z lewej na prawo.
		enemySpriteRenderer = this.gameObject.GetComponentInChildren<SpriteRenderer> ();
		enemyRigidBody = this.gameObject.GetComponent<Rigidbody2D> ();
		anim = this.gameObject.GetComponentInChildren<Animator> ();
		Destroy (pointToMove);
		damageHero = new DamageHero ();
	}


	void Update()
	{
		if (move) 
		{
			EnemyMove ();
		}
		else if (!move)
		{
			anim.SetBool ("EnemyWalk", false);
		}
	}

	void OnTriggerEnter2D(Collider2D collider)
	{
		if (collider.gameObject.tag == "AttackDistance") 
		{
			if (!getingDamage)
			{
				getingDamage = true;
				GetDamage ();
			}
		}
	}

	void OnTriggerStay2D(Collider2D collider)
	{
		if (collider.gameObject.tag == "Player" && !attackStart) 
		{
			Attack ();
		}
	}

	/// <summary>
	/// Klasa ta powoduję poruszanie się między punktem startowym postaci a wyznaczonym punktem.
	/// </summary>
	void EnemyMove()
	{
		enemyRigidBody.velocity = new Vector2 (moveSpeed * direction, enemyRigidBody.velocity.y);
		Vector2 positionNow = this.transform.position;
		if (positionNow.x > movePosition.x) 
		{
			enemySpriteRenderer.flipX = true;
			direction = -1;
			if (hitCollider.offset.x > 0)
			{
				hitCollider.offset = new Vector2 (hitCollider.offset.x * -1, hitCollider.offset.y);
			}
		}
		if(positionNow.x < startPosition.x)
		{
			enemySpriteRenderer.flipX = false;
			direction = 1;
			if (hitCollider.offset.x < 0)
			{
				hitCollider.offset = new Vector2 (hitCollider.offset.x * -1, hitCollider.offset.y);
			}
		}
		anim.SetBool ("EnemyWalk", true);

		//transform.position = Vector2.Lerp (startPosition, movePosition, Mathf.PingPong (Time.time * moveSpeed, 1));
	}

	private void Attack()
	{
		attackStart = true;
		anim.SetBool ("Attack", true);
		Invoke ("StopAttack", attackSpeed);
	}

	private void StopAttack()
	{
		attackStart = false;
		anim.SetBool ("Attack", false);
	}
		
	void GetDamage()
	{
		Debug.Log ("Przeciwnik otrzymał obrażenia");

		audioSource.clip = damageSound;
		audioSource.Play ();

		health--;
		move = false;

		ShowHealth (health);
		Instantiate (bloodEffect, transform.position, Quaternion.identity);
		enemyRigidBody.velocity = new Vector2 (0, enemyRigidBody.velocity.y);

		if (health <= 0) 	
		{
			Death ();
		} 
		else 
		{
			anim.SetBool ("GetDamage", true);
			Invoke ("EndGetDamage", getDamageTime);
		}
	}

	void ShowHealth(float health)
	{
		enemyHealth.SetActive (true);
		var childrenOfEnemyHealth = enemyHealth.GetComponentsInChildren<Transform> ();


		for (int i = 0; i < childrenOfEnemyHealth.Length; i++)
		{

			if (childrenOfEnemyHealth[i].gameObject.tag != "EnemyHealth")
			{
				Destroy (childrenOfEnemyHealth [i].gameObject);
			}
		}

		for (int i = 0; i < health; i++) 
		{
			GameObject placedObject = Instantiate (heart);
			var newPosition = enemyHealth.transform.position;
			newPosition.x = newPosition.x + i;
			placedObject.transform.position = newPosition;
			placedObject.transform.SetParent (enemyHealth.transform);
			Invoke ("HideHealth", 1);
		}
	}

	void HideHealth()
	{
		enemyHealth.SetActive (false);
	}

	void EndGetDamage()
	{
		getingDamage = false;
		this.GetComponentInChildren<Animator> ().SetBool ("GetDamage", false);
		move = true;
	}

	void Death()
	{
		move = false;
		this.gameObject.tag = "DeadEnemy";
		var tableOfCollidors = this.gameObject.GetComponents<BoxCollider2D> ();
		for (int i = 0; i < tableOfCollidors.Length; i++) 
		{
			if (tableOfCollidors [i].isTrigger == true) 
			{
				Destroy (tableOfCollidors [i]);
			}
		}

		anim.SetTrigger ("Dead");
	}
}
