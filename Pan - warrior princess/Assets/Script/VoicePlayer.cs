﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VoicePlayer : MonoBehaviour {

	public float waitTime;
	public AudioClip voiceToPlay;
	public AudioSource voiceAudio;
	public string scenToLoad;
	private bool checkEndOfVoice;

	// Use this for initialization
	void Start () 
	{
		Invoke("PlayVoice",waitTime);
	}

	void PlayVoice()
	{
		voiceAudio.clip = voiceToPlay;
		voiceAudio.Play ();
		checkEndOfVoice = true;
	}

	void Update()
	{
		if (checkEndOfVoice && !voiceAudio.isPlaying) 
		{
			ButtonPress loadScen = new ButtonPress ();
			loadScen.scenToLoad = scenToLoad;
			loadScen.turnOnLoadScreen = true;
			loadScen.LoadScen ();
		}
	}
}
