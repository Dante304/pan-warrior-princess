﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReturnMusic : MonoBehaviour {

	public AudioClip returnClip;
	public float volumeReturn;

	public void ReturnMusicClip()
	{
		MusicChanger musicChanger = new MusicChanger();
		musicChanger.musicToPlay = returnClip;
		musicChanger.volume = volumeReturn;
		musicChanger.ChangeMusic();
	}
}
