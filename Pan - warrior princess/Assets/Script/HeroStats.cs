﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class HeroStats : MonoBehaviour {

	#region Geter/Seter klasy
	/// <summary>
	/// Jest to właściwość służąca do ustawienia ilości aktualnych żyć bohaterki.
	/// </summary>
	/// <value>Ustawienie za pomocą set ilości serc. Zwrócenie ilości serc za pomocą get</value>
	public float GetSetNumberOfHearts
	{
		get
		{
			return numberOfHearts;
		}
		set
		{
			//TODO zamienić to na SWITCH by było bardziej optymalne
			if (value > 0 && value < numberOfHearts && !dead) 
			{
				Debug.Log ("trafiony: " + value);
				WoundingTheHero (value);
			}
			else if (value < 1 && !dead) 
			{
				DeathOfHero (value);
			}
			else if(value > 4)
			{
				Debug.Log ("Ilość serc próbuję przekroczyć wartość 4. Sprawdź ostatnią próbę set propertisu GetSetNumberOfHearts");
			}
			else if (value > numberOfHearts) 
			{
				HealTheHero (value);
			}
		}
	}

	void OnCollisionEnter2D(Collision2D collision)
	{
		if (collision.gameObject.tag ==  "DeadEnemy") 
		{
			Physics2D.IgnoreCollision (collision.gameObject.GetComponent<Collider2D>(), this.gameObject.GetComponent<Collider2D>());
		}
	}

	public float GetSetNumberOfEnergyBalls
	{
		get
		{
			return numberOfEnergyBall;
		}
		set
		{
			if (value > 0 && value < numberOfEnergyBall) 
			{
				Debug.Log ("Odjęto kule energii");
				//WoundingTheHero (value);
			}
			else if (value < 1) 
			{
				//DeathOfHero (value);
			}
			else if(value > 4)
			{
				Debug.Log ("Ilość kul energii próbuję przekroczyć wartość 3. Sprawdź ostatnią próbę set propertisu GetSetNumberOfEnergyBalls");
			}
			else if (value > numberOfEnergyBall) 
			{
				numberOfEnergyBall = value;
			}
		}
	}
	#endregion

	static public GameObject aboutHero;
	public GameObject placeForHearts;
	public GameObject placeForEnergyBalls;
	public GameObject heart;
	public GameObject energyBall;
	public float numberOfHearts;
	public float numberOfEnergyBall;
	public bool betterSword;
	public bool betterArmor;
	public bool dead;
	public GameObject deathScreen;
	public AudioClip deadSound;
	public float buttonShowTime;
	private List<GameObject> listOfHearts;
	private List<GameObject> listOfEnergyBalls;
	private Animator anim;
	private AudioSource audioSource;


	void Start () 
	{
		aboutHero = this.gameObject;
		audioSource = this.GetComponent<AudioSource> ();
		listOfHearts = ShowAdequateNumberOfObjects (numberOfHearts, placeForHearts, heart);
		listOfEnergyBalls = ShowAdequateNumberOfObjects (numberOfEnergyBall, placeForEnergyBalls, energyBall);
		anim = this.gameObject.GetComponentInChildren<Animator> ();
	}

	/// <summary>
	/// Metoda ta po wywołaniu wyświetla przyjętą w argumenicie liczbę obiektów w podanym obszarze. Obiekty te są zwracane w postaci listy obiektów. 
	/// </summary>
	private List<GameObject> ShowAdequateNumberOfObjects(float numberOfObjects,GameObject placeForObjects, GameObject objectToPlace)
	{
		List<GameObject> listOfPlacedObjects = new List<GameObject>();

		for (int i = 0; i < numberOfObjects; i++) 
		{
			GameObject placedObject = Instantiate (objectToPlace);
			placedObject.transform.SetParent (placeForObjects.transform);
			placedObject.transform.localScale = new Vector3(1f,1F,1F);
			listOfPlacedObjects.Add (placedObject);
		}

		return listOfPlacedObjects;
	}

	void DeathOfHero(float leftHearts)
	{
		dead = true;
		WoundingTheHero (leftHearts);
		audioSource.clip = deadSound;
		audioSource.Play ();
		Debug.Log("Pan została pokonana. Liczba serc spadła do zera");
		this.GetComponent<Rigidbody2D> ().velocity = new Vector2 (0, this.GetComponent<Rigidbody2D> ().velocity.y);
		anim.SetTrigger ("Death");  //Wywołuję w tym momencie animację śmierci bohaterki w ostatniej klatce animacji uruchamia się skrypt transportujący bohaterkę do punktu respawnu
		UiButtons.TurnOffButtons();
		Invoke ("Respawn",buttonShowTime);
		deathScreen.SetActive (true);
	}

	void Respawn()
	{
		this.GetComponent<HeroRespawn> ().Respawn ();
	}

	void WoundingTheHero(float leftHearts)
	{
		if (leftHearts < numberOfHearts) 
		{
			var heartsToDestroy = numberOfHearts - leftHearts;
			for (int i = 0; i < heartsToDestroy; i++) 
			{
				var number = listOfHearts.Count;
				Destroy (listOfHearts [number - 1].gameObject);
				listOfHearts.RemoveAt (number - 1);
			}
			numberOfHearts = leftHearts;
		}
	}

	void HealTheHero(float hearts)
	{
		numberOfHearts = hearts;
		if (numberOfHearts > listOfHearts.Count) 
		{
			var numberOfHeartsToAdd = numberOfHearts - listOfHearts.Count;
			listOfHearts = ShowAdequateNumberOfObjects (numberOfHeartsToAdd, placeForHearts, heart);
		}
	}

}
