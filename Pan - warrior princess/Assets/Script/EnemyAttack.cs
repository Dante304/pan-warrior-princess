﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAttack : MonoBehaviour 
{
	public GameObject hero;
	private DamageHero damageHero = new DamageHero();
	private Animator animator;

	void Start()
	{
		animator = this.GetComponent<Animator> ();
	}

	public void Damage()
	{

		if (this.GetComponentInParent<EnemyBase>().inRange)
		{
			damageHero.HitHero (hero);
		}
	}
}
